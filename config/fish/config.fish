if status is-interactive
end
set -x YT_API_KEY "AIzaSyDbqOVwAPOQXSs-kXuc7GHIYQjgE1U9sP0"
set -x GOPATH "$HOME/Go"
set -x PATH "$HOME/.local/bin/:$PATH"
set -x PATH "$HOME/.cargo/bin:$PATH"
set -x PATH "$GOPATH/bin:$PATH"
set -x EDITOR "/bin/nvim"
set -x SYSTEMD_EDITOR "/bin/nvim"
alias tlmgr="TEXMFDIST/scripts/texlive/tlmgr.pl --usermode"
alias discord="discord --enable-features=UseOzonePlatform --ozone-platform=wayland"
alias spotify="spotify --enable-features=UseOzonePlatform --ozone-platform=wayland"
alias vscodium="vscodium --enable-features=UseOzonePlatform --ozone-platform=wayland"
alias steam="steam --enable-features=UseOzonePlatform --ozone-platform=wayland"
alias signal-desktop="signal-desktop --enable-features=UseOzonePlatform --ozone-platform=wayland"
alias librewolf="librewolf -allow-downgrade"
alias ls="eza  --icons --git"
alias la="eza --long --all --header --bytes --git --icons -g"
fish_vi_key_bindings
starship init fish | source

set -q GHCUP_INSTALL_BASE_PREFIX[1]; or set GHCUP_INSTALL_BASE_PREFIX $HOME ; set -gx PATH $HOME/.cabal/bin /home/philip/.ghcup/bin $PATH # ghcup-env
