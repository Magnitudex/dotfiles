#!/bin/sh
nitrogen --restore &&
wal -R &
xidlehook --not-when-fullscreen --not-when-audio --timer 300 'betterlockscreen --lock blur' '' &
picom &
wal-dunst &
/usr/bin/lxpolkit &
caffeine &
clipit &

