local wezterm=require 'wezterm'
local mux = wezterm.mux

-- this is called by the mux server when it starts up.
-- It makes a window split top/bottom
return {
    font = wezterm.font 'Hack Nerd Font Mono',

    color_scheme = 'tokyonight_night',

    use_fancy_tab_bar = true,
    hide_tab_bar_if_only_one_tab = false,
    tab_bar_at_bottom = true,
    
    window_padding = {
	left = 0,
	right = 0,
	top = 0,
	bottom = 0,
    },
    unix_domains = {
	{
	    name = 'unix',
	},
    },
    disable_default_key_bindings = true,
    leader = {key = 'Space', mods = 'CTRL'},
    keys = {
	{key = '1', mods = 'LEADER', action = wezterm.action.ActivateTab(0)},
	{key = '2', mods = 'LEADER', action = wezterm.action.ActivateTab(1)},
	{key = '3', mods = 'LEADER', action = wezterm.action.ActivateTab(2)},
	{key = '4', mods = 'LEADER', action = wezterm.action.ActivateTab(3)},
	{key = '5', mods = 'LEADER', action = wezterm.action.ActivateTab(4)},
	{key = '6', mods = 'LEADER', action = wezterm.action.ActivateTab(5)},
	{key = '7', mods = 'LEADER', action = wezterm.action.ActivateTab(6)},
	{key = '8', mods = 'LEADER', action = wezterm.action.ActivateTab(7)},
	{key = '9', mods = 'LEADER', action = wezterm.action.ActivateTab(8)},
	{key = 'Tab', mods = 'ALT', action = wezterm.action.ActivateTabRelative(1)},
	{key = 'v', mods = 'LEADER', action = wezterm.action.SplitVertical{domain = "CurrentPaneDomain"}},
	{key = 'h', mods = 'LEADER', action = wezterm.action.SplitHorizontal{domain = "CurrentPaneDomain"}},
	{key = 'v', mods = 'CTRL|SHIFT', action = wezterm.action.PasteFrom("Clipboard")},
	{key = 'c', mods = 'CTRL|SHIFT', action = wezterm.action.CopyTo("Clipboard")},
    {key = 'P',mods = 'CTRL',action = wezterm.action.ActivateCommandPalette},
    },
    serial_ports = {
        {
            name = 'rpi',
            port = '/dev/ttyUSB0',
            baud = 115200
        },
    },

  -- This causes `wezterm` to act as though it was started as
  -- `wezterm connect unix` by default, connecting to the unix
  -- domain on startup.
  -- If you prefer to connect manually, leave out this line.
  default_gui_startup_args = { 'connect', 'unix' },
   -- window_background_image = '/home/philip/Downloads/background.png'
}

