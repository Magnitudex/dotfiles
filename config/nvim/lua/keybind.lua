--leader key
vim.g.mapleader = ','

--nvim built in functions
vim.keymap.set('n', '<Leader>vs', ':vsplit<CR>' )
vim.keymap.set('n', '<Leader>hs', ':split<CR>' )

--Telescope keybindings
local builtin = require('telescope.builtin')
vim.keymap.set('n', '<leader>ff', builtin.find_files, { desc = 'Telescope find files' })
vim.keymap.set('n', '<leader>fg', builtin.live_grep, { desc = 'Telescope live grep' })
vim.keymap.set('n', '<leader>fb', builtin.buffers, { desc = 'Telescope buffers' })
vim.keymap.set('n', '<leader>fh', builtin.help_tags, { desc = 'Telescope help tags' })

--LazyGitConfig
vim.keymap.set('n','<leader>gg',':LazyGit<CR>')

--buffer movement
vim.keymap.set('n', '<leader>bh',':wincmd h<CR>')
vim.keymap.set('n', '<leader>bj',':wincmd j<CR>')
vim.keymap.set('n', '<leader>bk',':wincmd k<CR>')
vim.keymap.set('n', '<leader>bl',':wincmd l<CR>')

--FTerm
vim.keymap.set('n', '<leader>ft', '<CMD>lua require("FTerm").toggle()<CR>')
vim.keymap.set('t', '<leader>ft', '<C-\\><C-n><CMD>lua require("FTerm").toggle()<CR>')
